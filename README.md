# QCrop

A basic PyQt image cropping tool.

## Install

```
pip install qcrop
```

## Usage

### As an application
Start with the command:
```
qcrop path/to/image.ext
```

The supported image formats are [those handled by the Qt framework][formats]: BMP, GIF, JPG, PNG, PBM, PGM, PPM, XBM and XPM.

### As a python module
Sample code:

```python
from PyQt5.QtGui import QPixmap
from qcrop.ui import QCrop

original_image = QPixmap(...)
crop_tool = QCrop(original_image)
status = crop_tool.exec()

if status == Qt.Accepted:
    cropped_image = crop_tool.image
# else crop_tool.image == original_image
```

### User interface
Use the mouse to select the image area to crop. You can also use the X, Y, Width and Height fields to precisely adjust the area (you can use mouse wheel hovering the field).

Click the reset button to set the crop area to the full image.

Once done click ok to validate the crop, cancel to go back to the original image.

![screenshot](screenshot.png)


   [formats]: https://doc.qt.io/qt-5/qpixmap.html#reading-and-writing-image-files
